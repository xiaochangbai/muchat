package com.xchb.im.conne.config;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class RabbitAMQPConfigTest {


    @Autowired
    private AmqpTemplate amqpTemplate;


    @Test
    public void test(){
        amqpTemplate.convertAndSend("muchat-test", "aaaaaa");
        System.out.println(amqpTemplate);
    }

}