package com.xchb.im.conne.config;

import com.xchb.im.common.config.RabbitMQConst;
import com.xiaoleilu.hutool.util.IdcardUtil;
import org.checkerframework.checker.units.qual.A;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/22
 * @description Good Good Study,Day Day Up.
 */
@Configuration
public class RabbitMQConfig {

    @Autowired
    private QueueName queueName;

    @Bean
    public Queue muchatChatQueue() {
        return QueueBuilder.nonDurable(queueName.build())
                .autoDelete().build();
    }

    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public TopicExchange muchatChatExchange(){
        return ExchangeBuilder.topicExchange(RabbitMQConst.CHAT_MESSAGE_EXCHANGE).build();
    }


    @Bean
    public Binding binding(){
        return BindingBuilder.bind(muchatChatQueue()).to(muchatChatExchange()).with(RabbitMQConst.CHAT_MESSAGE_ROUTINGKEY);
    }


}
