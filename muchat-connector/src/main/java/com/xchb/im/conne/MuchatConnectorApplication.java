package com.xchb.im.conne;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/21
 * @description Good Good Study,Day Day Up.
 */

@EnableRabbit
@SpringBootApplication
public class MuchatConnectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(MuchatConnectorApplication.class,args);
    }

}
