package com.xchb.im.conne.pojo;

import lombok.Data;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
@Data
public class LoginInfo {

    private String userId;
}
