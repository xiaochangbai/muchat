package com.xchb.im.conne.init;

import com.xchb.im.conne.server.netty.WSServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ServerBooter implements ApplicationListener<ContextRefreshedEvent> {

	@Value("${app.ws.port}")
	private Integer port;


	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			try {
				WSServer.getInstance().start(port);
				log.info("Netty websocket server 启动完毕,Port = {}",port);
			} catch (Exception e) {
				log.info("netty websocket server 启动失败,",e);
			}
		}
	}
	
}
