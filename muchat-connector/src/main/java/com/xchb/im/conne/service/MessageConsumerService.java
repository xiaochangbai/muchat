package com.xchb.im.conne.service;

import com.alibaba.fastjson.JSONObject;
import com.xchb.im.common.enums.NocticeTypeEnum;
import com.xchb.im.common.pojo.ChatMsg;
import com.xchb.im.common.pojo.NoticeContent;
import com.xchb.im.conne.config.RabbitMQConfig;
import com.xchb.im.conne.utils.UserConnectionHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author XDD
 * @project muchat
 * @date 2021/7/22
 * @description Good Good Study,Day Day Up.
 */
@Component
@Slf4j
public class MessageConsumerService {

    @Autowired
    private UserConnectionHolder userConnectionHolder;

    @RabbitListener(queues = "#{queueName.build()}")
    public void consumer(NoticeContent noticeContent){
        log.info("收到消息：{}",noticeContent);
        if(noticeContent.getAction()== NocticeTypeEnum.CHAT.type){
            ChatMsg chatMsg = noticeContent.getChatMsg();
            userConnectionHolder.send(chatMsg.getAcceptUserId(),chatMsg.getMsg());
        }
    }


}
