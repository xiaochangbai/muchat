package com.xchb.im.conne.config;

import com.xchb.im.common.config.RabbitMQConst;
import org.springframework.stereotype.Component;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/23
 * @description Good Good Study,Day Day Up.
 */
@Component
public class QueueName {

    private final static String seed = String.valueOf(System.currentTimeMillis());

    public String build() {
        return RabbitMQConst.QUEUE_NAME_PRE+seed;
    }
}