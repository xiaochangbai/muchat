package com.xchb.im.conne.server.netty;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataContent implements Serializable {

	private static final long serialVersionUID = 8021381444738260454L;

	private Integer action;		// 动作类型
	private ChatMsg chatMsg;	// 用户的聊天内容entity
	private String extand;		// 扩展字段
	
	public Integer getAction() {
		return action;
	}
	public void setAction(Integer action) {
		this.action = action;
	}
	public ChatMsg getChatMsg() {
		return chatMsg;
	}
	public void setChatMsg(ChatMsg chatMsg) {
		this.chatMsg = chatMsg;
	}
	public String getExtand() {
		return extand;
	}
	public void setExtand(String extand) {
		this.extand = extand;
	}
	
    public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		String content = "{\"action\":1,\"extand\":\"1324\"}";
		DataContent result = mapper.readValue(content, DataContent.class);
		String ss = mapper.writeValueAsString(result);
		System.out.println(result);
		
		System.out.println(ss);
	}
	@Override
	public String toString() {
		return "DataContent [action=" + action + ", chatMsg=" + chatMsg + ", extand=" + extand + "]";
	}
    
    
}
