

## 介绍

`MuChat`，中文：慕聊。
 一款基于Netty+MQ的高性能IM解决方案，天然的支持水平扩展。


## 系统架构
![](doc/高性能IM架构.jpg)


## 核心技术

- SpringBoot: 项目基础环境构建与集成其他框架。
- SpringMVC：HTTP请求处理。
- Netty：与客户端建立长连接。
- Zookeepr: 服务注册与发现。
- RabbitMq: 消息通道。


## TODO LIST


* [x] [私聊](#私聊)
* [x] 根据实际情况灵活的水平扩容、缩容
* [ ] [群聊](#群聊)
* [ ] 使用 `Google Protocol Buffer` 高效编解码
* [ ] 客户端自动重连
* [ ] 采用Hazelcast重构







**支持集群部署。**



## 流程图


- 客户端向 `muchat-server` 发起登录。
- 登录成功从 `Zookeeper` 中选择可用 `muchat-server` 返回给客户端，并保存登录、路由信息到 `Redis`。
- 客户端向 `muchat-server` 发起长连接，成功后保持心跳。
- 客户端下线时通过 `route` 清除状态信息。


## 快速启动

首先需要安装 `Zookeeper、Redis、RabbitMQ` 并保证网络通畅。
