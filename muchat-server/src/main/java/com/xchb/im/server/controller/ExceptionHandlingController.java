package com.xchb.im.server.controller;


import com.xchb.im.common.exception.MuChatException;
import com.xchb.im.server.utils.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 全局异常处理类
 */
@ControllerAdvice
public class ExceptionHandlingController {

    private static Logger logger = LoggerFactory.getLogger(ExceptionHandlingController.class) ;

    @ExceptionHandler(MuChatException.class)
    @ResponseBody()
    public JSONResult handleAllExceptions(MuChatException ex) {
        logger.error("exception", ex);
        JSONResult baseResponse = new JSONResult();
        baseResponse.setStatus(ex.getErrorCode());
        baseResponse.setMsg(ex.getMessage());
        return baseResponse ;
    }

}