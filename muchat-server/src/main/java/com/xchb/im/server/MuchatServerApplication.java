package com.xchb.im.server;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import org.springframework.context.annotation.ComponentScan;
import tk.mybatis.spring.annotation.MapperScan;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@MapperScan(basePackages="com.xchb.im.server.mapper")
@ComponentScan(basePackages= {"com.xchb"})
public class MuchatServerApplication implements ApplicationListener<ApplicationReadyEvent> {


	@Value("${server.port}")
	private Integer port;

	public static void main(String[] args) {
		SpringApplication.run(MuchatServerApplication.class, args);
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		try {
			String ip = InetAddress.getLocalHost().getHostAddress();
			System.out.println("\n接口文档地址：http://"+ip+":"+port+"/doc.html");
			System.out.println("接口文档地址：http://localhost:"+port+"/doc.html\n");
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
