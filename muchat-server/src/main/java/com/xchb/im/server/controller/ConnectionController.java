package com.xchb.im.server.controller;

import com.xchb.im.server.config.AppConst;
import com.xchb.im.server.entity.po.NodeInfoVo;
import com.xchb.im.server.service.ConnectionService;
import com.xchb.im.server.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
@Api(tags = "连接信息")
@RestController
public class ConnectionController {


    @Autowired
    private ConnectionService connectionService;

    @ApiOperation("查看所有在线的长连接机器")
    @GetMapping("/conneList")
    public JSONResult conneList(){
        List<NodeInfoVo> nodeInfoVoList = connectionService.nodeList();
        return JSONResult.ok(nodeInfoVoList);
    }


    @ApiOperation("选择一台可用的长连接")
    @GetMapping("/node")
    public JSONResult node(){
        NodeInfoVo nodeInfoVo = connectionService.node();
        if(nodeInfoVo==null){
            return JSONResult.errorMsg("没有可用的服务");
        }
        return JSONResult.ok(nodeInfoVo);
    }





}
