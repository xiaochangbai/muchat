package com.xchb.im.server.service;

import com.xchb.im.server.config.AppConst;
import com.xchb.im.server.entity.po.NodeInfoVo;

import java.util.List;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
public interface ConnectionService {

    List<NodeInfoVo> nodeList();

    NodeInfoVo node();
}
