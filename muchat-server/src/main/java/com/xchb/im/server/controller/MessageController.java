package com.xchb.im.server.controller;

import com.xchb.im.common.enums.MsgType;
import com.xchb.im.common.enums.NocticeTypeEnum;
import com.xchb.im.common.pojo.ChatMsg;
import com.xchb.im.common.pojo.NoticeContent;
import com.xchb.im.server.service.MessageService;
import com.xchb.im.server.utils.JSONResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
@Api(tags = "消息模块")
@RestController
public class MessageController {

    @Autowired
    private MessageService messageService;

    @ApiOperation("发送消息")
    @PostMapping("/send")
    public JSONResult send(@RequestBody ChatMsg chatMsg){
        if(messageService.send(chatMsg)){
            return JSONResult.ok("发送成功");
        }
        return JSONResult.errorMsg("消息类型错误");
    }



}
