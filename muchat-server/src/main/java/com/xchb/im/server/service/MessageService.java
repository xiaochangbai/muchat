package com.xchb.im.server.service;

import com.xchb.im.common.pojo.ChatMsg;
import com.xchb.im.common.pojo.NoticeContent;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/23
 * @description Good Good Study,Day Day Up.
 */
public interface MessageService {

    public boolean send(ChatMsg chatMsg);
}
