package com.xchb.im.server.mapper;

import com.xchb.im.server.entity.po.Users;
import com.xchb.im.server.utils.MyMapper;

public interface UsersMapper extends MyMapper<Users> {
}