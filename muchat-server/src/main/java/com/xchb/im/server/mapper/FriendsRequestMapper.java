package com.xchb.im.server.mapper;


import com.xchb.im.server.entity.po.FriendsRequest;
import com.xchb.im.server.utils.MyMapper;

public interface FriendsRequestMapper extends MyMapper<FriendsRequest> {
}