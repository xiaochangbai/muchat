package com.xchb.im.server.mapper;

import com.xchb.im.common.pojo.ChatMsg;
import com.xchb.im.server.utils.MyMapper;

public interface ChatMsgMapper extends MyMapper<ChatMsg> {
}