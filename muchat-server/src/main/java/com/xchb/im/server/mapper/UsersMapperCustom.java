package com.xchb.im.server.mapper;

import java.util.List;

import com.xchb.im.server.entity.po.Users;
import com.xchb.im.common.pojo.vo.FriendRequestVO;
import com.xchb.im.common.pojo.vo.MyFriendsVO;
import com.xchb.im.server.utils.MyMapper;

public interface UsersMapperCustom extends MyMapper<Users> {
	
	public List<FriendRequestVO> queryFriendRequestList(String acceptUserId);
	
	public List<MyFriendsVO> queryMyFriends(String userId);
	
	public void batchUpdateMsgSigned(List<String> msgIdList);
	
}