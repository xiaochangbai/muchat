package com.xchb.im.server.mapper;

import com.xchb.im.server.entity.po.MyFriends;
import com.xchb.im.server.utils.MyMapper;

public interface MyFriendsMapper extends MyMapper<MyFriends> {
}