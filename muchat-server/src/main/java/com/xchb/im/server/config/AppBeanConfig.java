package com.xchb.im.server.config;

import com.xchb.im.common.algorithm.RouteHandle;
import com.xchb.im.common.algorithm.consistenthash.ConsistentHashHandle;
import com.xchb.im.common.algorithm.consistenthash.TreeMapConsistentHash;
import com.xchb.im.common.algorithm.random.RandomHandle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
@Configuration
public class AppBeanConfig {

    @Bean
    public RouteHandle routeHandle(){
        ConsistentHashHandle hashHandle = new ConsistentHashHandle();
        hashHandle.setHash(new TreeMapConsistentHash());
        return hashHandle;
    }
}
