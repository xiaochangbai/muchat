package com.xchb.im.common.exception;


import com.xchb.im.common.enums.StatusEnum;

/**
 * Function:
 *
 * @author crossoverJie
 *         Date: 2018/8/25 15:26
 * @since JDK 1.8
 */
public class MuChatException extends GenericException {


    public MuChatException(Integer errorCode, String errorMessage) {
        super(errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public MuChatException(Exception e, Integer errorCode, String errorMessage) {
        super(e, errorMessage);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public MuChatException(String message) {
        super(message);
        this.errorMessage = message;
    }

    public MuChatException(StatusEnum statusEnum) {
        super(statusEnum.getMessage());
        this.errorMessage = statusEnum.message();
        this.errorCode = statusEnum.getCode();
    }

    public MuChatException(StatusEnum statusEnum, String message) {
        super(message);
        this.errorMessage = message;
        this.errorCode = statusEnum.getCode();
    }

    public MuChatException(Exception oriEx) {
        super(oriEx);
    }

    public MuChatException(Throwable oriEx) {
        super(oriEx);
    }

    public MuChatException(String message, Exception oriEx) {
        super(message, oriEx);
        this.errorMessage = message;
    }

    public MuChatException(String message, Throwable oriEx) {
        super(message, oriEx);
        this.errorMessage = message;
    }


    public static boolean isResetByPeer(String msg) {
        if ("Connection reset by peer".equals(msg)) {
            return true;
        }
        return false;
    }

}
