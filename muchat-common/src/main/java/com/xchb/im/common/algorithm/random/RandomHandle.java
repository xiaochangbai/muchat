package com.xchb.im.common.algorithm.random;


import com.xchb.im.common.algorithm.RouteHandle;
import com.xchb.im.common.enums.StatusEnum;
import com.xchb.im.common.exception.MuChatException;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Function: 路由策略， 随机
 */
public class RandomHandle implements RouteHandle {

    @Override
    public String routeServer(List<String> values, String key) {
        int size = values.size();
        if (size == 0) {
            throw new MuChatException(StatusEnum.SERVER_NOT_AVAILABLE) ;
        }
        int offset = ThreadLocalRandom.current().nextInt(size);

        return values.get(offset);
    }


}
