package com.xchb.im.common.enums;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/24
 * @description Good Good Study,Day Day Up.
 */
public enum  MsgType {


    privat(0, "私聊"),
    group(1, "群聊");

    public final Integer type;
    public final String content;

    MsgType(Integer type, String content){
        this.type = type;
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

}
