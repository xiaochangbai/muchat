package com.xchb.im.common.algorithm;

import java.util.List;

/**
 * Function:
 */
public interface RouteHandle {

    /**
     * 再一批服务器里进行路由
     * @param values
     * @param key
     * @return
     */
    String routeServer(List<String> values, String key) ;
}
