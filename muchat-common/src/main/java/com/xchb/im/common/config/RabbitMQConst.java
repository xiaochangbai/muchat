package com.xchb.im.common.config;

/**
 * @author XDD
 * @project muchat
 * @date 2021/7/23
 * @description Good Good Study,Day Day Up.
 */
public interface RabbitMQConst {


    public static String CHAT_MESSAGE_EXCHANGE = "chat.exchange";
    public static String CHAT_MESSAGE_ROUTINGKEY = "chat.queues.*";
    public final static String QUEUE_NAME_PRE = "chat.queue.";
}
