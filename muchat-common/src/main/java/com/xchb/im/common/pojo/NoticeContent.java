package com.xchb.im.common.pojo;


import lombok.Data;

import java.io.IOException;
import java.io.Serializable;

@Data
public class NoticeContent implements Serializable {

	private static final long serialVersionUID = 8021381444738260454L;

	private Integer action;		// 动作类型
	private ChatMsg chatMsg;	// 用户的聊天内容
	private String extand;		// 扩展字段


	@Override
	public String toString() {
		return "DataContent [action=" + action + ", chatMsg=" + chatMsg + ", extand=" + extand + "]";
	}
    
    
}
