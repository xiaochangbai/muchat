package com.xchb.im.common.pojo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;


@Data
@ApiModel
public class ChatMsg {


    @ApiModelProperty("消息id")
    private String id;

    @ApiModelProperty("类型：0私聊，1群聊")
    private Integer type;

    @ApiModelProperty("发送者id")
    private String sendUserId;

    @ApiModelProperty("接收者id")
    private String acceptUserId;

    @ApiModelProperty("消息内容")
    private String msg;

    /**
     * 消息是否签收状态
        1：签收
        0：未签收
     */
    private Integer signFlag;

    /**
     * 发送请求的事件
     */
    private Date createTime;


}